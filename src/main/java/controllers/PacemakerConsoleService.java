package controllers;

import com.bethecoder.ascii_table.ASCIITable;
import com.bethecoder.ascii_table.impl.CollectionASCIITableAware;
import com.bethecoder.ascii_table.spec.IASCIITableAware;
import com.google.common.base.Optional;

import asg.cliche.Command;
import asg.cliche.Param;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import models.Activity;
import models.User;
import parsers.AsciiTableParser;
import parsers.Parser;

public class PacemakerConsoleService {

  private PacemakerAPI paceApi = new PacemakerAPI("https://pacemaker-annab.herokuapp.com");
  private Parser console = new AsciiTableParser();
  private User loggedInUser = null;

  public PacemakerConsoleService() {
  }

  // Starter Commands

  @Command(description = "Register: Create an account for a new user")
  public void register(@Param(name = "first name") String firstName,
      @Param(name = "last name") String lastName,
      @Param(name = "email") String email, @Param(name = "password") String password) {
    console.renderUser(paceApi.createUser(firstName, lastName, email, password));
  }

  @Command(description = "List Users: List all users emails, first and last names")
  public void listUsers() {
    console.renderUsers(paceApi.getUsers());
  }

  @Command(description = "Login: Log in a registered user in to pacemaker")
  public void login(@Param(name = "email") String email,
      @Param(name = "password") String password) {
    Optional<User> user = Optional.fromNullable(paceApi.getUserByEmail(email));
    if (user.isPresent()) {
      if (user.get().password.equals(password)) {
        loggedInUser = user.get();
        console.println("Logged in " + loggedInUser.email);
        console.println("ok");
      } else {
        console.println("Error on login");
      }
    }
  }

  @Command(description = "Logout: Logout current user")
  public void logout() {
    console.println("Logging out " + loggedInUser.email);
    console.println("ok");
    loggedInUser = null;
  }

  @Command(description = "Add activity: create and add an activity for the logged in user")
  public void addActivity(
      @Param(name = "type") String type,
      @Param(name = "location") String location,
      @Param(name = "distance") double distance) {
    Optional<User> user = Optional.fromNullable(loggedInUser);
    if (user.isPresent()) {
      console
          .renderActivity(paceApi.createActivity(user.get().id, type, location, distance));
    }
  }

  @Command(description = "List Activities: List all activities for logged in user")
  public void listActivities() {
    Optional<User> user = Optional.fromNullable(loggedInUser);
    if (user.isPresent()) {
      console
          .renderActivities(paceApi.getActivities(user.get().id));
    }
  }

  // Baseline Commands

  @Command(description = "Add location: Append location to an activity")
  public void addLocation(@Param(name = "activity-id") String id,
      @Param(name = "longitude") double longitude, @Param(name = "latitude") double latitude) {
    Optional<Activity> activity = Optional.fromNullable(paceApi.getActivity(loggedInUser.getId()));
    if (activity.isPresent()) {
      paceApi.addLocation(loggedInUser.getId(), activity.get().id, latitude, longitude);
      console.println("ok");
    } else {
      console.println("not found");
    }
  }

  @Command(description = "ActivityReport: List all activities for logged in user, sorted alphabetically by type")
  public void activityReport() {
    Optional<User> user = Optional.fromNullable(loggedInUser);
    if (user.isPresent()) {
      console.renderActivities(paceApi.listActivities(user.get().id, "type"));
    }
  }

  @Command(
	      description = "Activity Report: List all activities for logged in user by type. Sorted longest to shortest distance")
	  public void activityReport(@Param(name = "byType: type") String type) {
	    Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      List<Activity> reportActivities = new ArrayList<>();
	      List<Activity> reportActivities1 = new ArrayList<>();
	      Collection<Activity> usersActivities = paceApi.getActivities(user.get().id);
	      usersActivities.forEach(a -> {
	        if (a.type.equals(type))
	          reportActivities.add(a);
	      });
	      reportActivities1 = reportActivities.stream().sorted(Comparator.comparing(Activity::getDistance).reversed()).collect(Collectors.toList());
	    //  reportActivities.sort((a1, a2) -> a1.getDistance().compareTo(a2.getDistance()));
	      console.renderActivities(reportActivities1);
	    }
	  }
  
  @Command(description = "List all locations for a specific activity")
  public void listActivityLocations(@Param(name = "activity-id") String id) {

    Optional<Activity> activity = Optional.fromNullable(paceApi.getActivity(loggedInUser.getId()));
    if (activity.isPresent()) {
      // console.renderLocations(activity.get().route);
    }
  }
  
  @Command(description = "Follow Friend: Follow a specific friend")
  public void follow(@Param(name = "email") String email) {
	    Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      paceApi.createFriendship(user.get().id, email);
	      Collection<User> friends =  user.get().friendship;
	      System.out.println("Adding friend " +email);
	      console.renderUsers(friends);
	    }
  }
  

  @Command(description = "List Friends: List all of the friends of the logged in user")
  public void listFriends() {
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	      console.renderUsers(friends);
	    }  
  }

  @Command(description = "Friend Activity Report: List all activities of specific friend, sorted alphabetically by type)")
  public void friendActivityReport(@Param(name = "email") String email) {
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	       friends.forEach(f -> {
	       if (f.email.equals(email))
	    	   System.out.println("These are all activities for friend " +f.getFirstname());
	    	   console.renderActivities(paceApi.listActivities(f.getId(), "type"));
	       });
	    }
  }

  // Good Commands

  @Command(description = "Unfollow Friends: Stop following a friend")
  public void unfollowFriend(@Param(name = "email") String email) {
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	       for (User friend : friends) {
	    	 if (friend.email.contains(email)) {
	    	   System.out.println("Removing from the list of your friends: " +friend.getFirstname());
	    	   friends.remove(friend);
	    	   console.renderUsers(friends);
	    	   break;
	    	 }
	    	 else {
	    	   System.out.println("You don't have this person in your friends list");
	    	   break;
	    	 }
	       }
	    }
  }

  @Command(description = "Message Friend: send a message to a friend")
  public void messageFriend(@Param(name = "email") String email,
      @Param(name = "message") String message) {
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	      for (User friend : friends) {
	    	 if (friend.email.contains(email)) {
	    		 System.out.println("You're sending this message: " + message +
	    				            "\n to: "+ friend.getFirstname());
	    		   User friendUser = paceApi.getUserByEmail(email);
		    	   friendUser.messages.add(message);
	    		   user.get().messages.add(message);
	    		   listMessages();
		       }
	    }
  }}
	    	 

  @Command(description = "List Messages: List all messages for the logged in user")
  public void listMessages() {
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	    		   List<String> userMessages = user.get().messages;
	    		   userMessages.forEach(System.out::println);
	    		   }
  }

  @Command(description = "Distance Leader Board: list summary distances of all friends, sorted longest to shortest")
  public void distanceLeaderBoard() {
	  ArrayList<User> userList = new ArrayList<User>();
	  ArrayList<Activity> activities = new ArrayList<Activity>();
	  Map<String, Double> finalDistances = new HashMap<String, Double>();
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	       friends.forEach(f -> {
	       double sum = 0;
	       //if (f.email.equals(email))
	    	   Collection<Activity> friendsActivities = paceApi.getActivities(f.getId());
	    	   for (Activity fa : friendsActivities) {
	    		   sum += fa.distance;
	    	   }
	       finalDistances.put(f.getEmail(), sum);
	    });
	       finalDistances.entrySet().stream()
	       .sorted(Map.Entry.<String, Double>comparingByValue().reversed()) 
	       .forEach(s -> {
	    	   User friend = new User("", "" ,  s.getKey() , "");
	    	   Activity friendsActivity = new Activity( "","", s.getValue());
	    	   userList.add(friend);
	    	   friend.activities.put(friend.getEmail(),friendsActivity);
	    	   activities.add(friendsActivity);
	         });
	        IASCIITableAware asciiTableAware = new CollectionASCIITableAware<User>(userList, "email");
	        System.out.printf(ASCIITable.getInstance().getTable(asciiTableAware));
	        IASCIITableAware asciiTableAware1 = new CollectionASCIITableAware<Activity>(activities,"distance");
	        ASCIITable.getInstance().printTable( asciiTableAware1);
	        
	      }
	      System.out.println("ok");
  }

  // Excellent Commands

  @Command(description = "Distance Leader Board: distance leader board refined by type")
  public void distanceLeaderBoardByType(@Param(name = "byType: type") String type) {
	  System.out.println("Distance leader board by type: " +type);
	  ArrayList<User> userList = new ArrayList<User>();
	  ArrayList<Activity> activities = new ArrayList<Activity>();
	  Map<String, Double> finalDistances = new HashMap<String, Double>();
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	       friends.forEach(f -> {
	    	   List<Double> distances = new ArrayList<>();
	    		   f.activities.values().forEach(action -> {
	    			   if ((action.getType() != null && action.getType().equals(type))) {
	    				   distances.add(action.distance);
	    		    	   }
	    			   double sum = distances.stream().reduce(0.0, Double::sum);
	    		       finalDistances.put(f.getEmail(), sum);
	    		       sum = 0;
	    		    });
	       });
	    		       finalDistances.entrySet().stream()
	    		       .sorted(Map.Entry.<String, Double>comparingByValue().reversed()) 
	    		       .forEach(s -> {
	    		    	   User friend = new User("", "" ,  s.getKey() , "");
	    		    	   Activity friendsActivity = new Activity( "","", s.getValue());
	    		    	   userList.add(friend);
	    		    	   friend.activities.put(friend.getEmail(),friendsActivity);
	    		    	   activities.add(friendsActivity);
	    		         });
	    		        IASCIITableAware asciiTableAware = new CollectionASCIITableAware<User>(userList, "email");
	    		        System.out.printf(ASCIITable.getInstance().getTable(asciiTableAware));
	    		        IASCIITableAware asciiTableAware1 = new CollectionASCIITableAware<Activity>(activities,"distance");
	    		        ASCIITable.getInstance().printTable( asciiTableAware1);
	    			   }  
}
	   

  @Command(description = "Message All Friends: send a message to all friends")
  public void messageAllFriends(@Param(name = "message") String message) {
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	       friends.forEach(f -> {
	    	   f.messages.add(message);
	       });
	       user.get().messages.add(message);
	       listMessages();
	    }
  }

  @Command(description = "Location Leader Board: list sorted summary distances of all friends in named location")
  public void locationLeaderBoard(@Param(name = "location") String location) {
	  System.out.println("Distance leader board by location: " + location);
	  ArrayList<User> userList = new ArrayList<User>();
	  ArrayList<Activity> activities = new ArrayList<Activity>();
	  Map<String, Double> finalDistances = new HashMap<String, Double>();
	  Optional<User> user = Optional.fromNullable(loggedInUser);
	    if (user.isPresent()) {
	      Collection<User> friends =  user.get().friendship;
	       friends.forEach(f -> {
	    	   List<Double> distances = new ArrayList<>();
	    		   f.activities.values().forEach(action -> {
	    			   if ((action.getLocation() != null && action.getLocation().equals(location))) {
	    				   distances.add(action.distance);
	    		    	   }
	    			   double sum = distances.stream().reduce(0.0, Double::sum);
	    		       finalDistances.put(f.getEmail(), sum);
	    		       sum = 0;
	    		    });
	       });
	    		       finalDistances.entrySet().stream()
	    		       .sorted(Map.Entry.<String, Double>comparingByValue().reversed()) 
	    		       .forEach(s -> {
	    		    	   User friend = new User("", "" ,  s.getKey() , "");
	    		    	   Activity friendsActivity = new Activity( "","", s.getValue());
	    		    	   userList.add(friend);
	    		    	   friend.activities.put(friend.getEmail(),friendsActivity);
	    		    	   activities.add(friendsActivity);
	    		         });
	    		        IASCIITableAware asciiTableAware = new CollectionASCIITableAware<User>(userList, "email");
	    		        System.out.printf(ASCIITable.getInstance().getTable(asciiTableAware));
	    		        IASCIITableAware asciiTableAware1 = new CollectionASCIITableAware<Activity>(activities,"distance");
	    		        ASCIITable.getInstance().printTable( asciiTableAware1);
	    			   }  
  }
  

  // Outstanding Commands
  @Command(description = "Login: Log in the administrator", abbrev = "lad")
  public void loginAdmin(@Param(name = "email") String email,
      @Param(name = "password") String password) {
	  System.out.println("Admin login " + email + " " + password);
	    Optional<User> user = Optional.fromNullable(paceApi.getUserByEmail(email));
	    if (user.isPresent()) {
	      if ( user.get().email.equals("admin@admin.com")) {
		      if (user.get().password.equals(password)) {
		        loggedInUser = user.get();
		        console.println("Logged in administrator");
		        console.println("ok");
		      }
		      else {
		    	  console.println("Error on login"); 
		      }
	      }
	      else {
	        console.println("Error on login");
	      }
	    }
	    else {
	    	 console.println("Error on login");
	    }
	  }

  @Command(description = "Admin: Delete user by id")
  public void deleteUser(@Param(name = "id") String id) {
	    User admin = paceApi.getUserByEmail("admin@admin.com");
	    Optional<User> adminUser = Optional.fromNullable(loggedInUser);  
		if (adminUser.isPresent()) {
		   if ( adminUser.get().email.equals("admin@admin.com")) {
	         paceApi.deleteUser(id);
	         System.out.println("User deleted");
		   }
		   else
		   {
			 System.out.println("You don't have admin permission");
		   }
		}
		else {
		  System.out.println("You don't have admin permission");
		} 
  }
  // Todo
}

