package controllers;

import static models.Fixtures.activities;
import static models.Fixtures.users;
import static models.Fixtures.locations;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Optional;

import models.Activity;
import models.User;
import controllers.PacemakerConsoleService;

public class PacemakerConsoleServiceTest {
	
	  private PacemakerAPI pacemaker;
	  private PacemakerConsoleService service;

	  @Before
	  public void setup()
	  {
		PacemakerAPI pacemaker = new PacemakerAPI("https://pacemaker-annab.herokuapp.com");
		PacemakerConsoleService service = new PacemakerConsoleService();
	    pacemaker.deleteUsers();
	    users.forEach(user -> pacemaker.createUser(user.firstname, user.lastname, user.email, user.password));
	    
	  }

	  @After
	  public void tearDown()
	  {
	    pacemaker = null;
	  }
	  
	  @Test
	  public void testRegister()
	  {
		User homer = new User ("homer", "simpson", "homer@simpson.com", "secret");
		assertEquals (users.size(), pacemaker.getUsers().size());
		service.register("homer", "simpson", "homer@simpson.com", "secret");
		User user2 = pacemaker.getUserByEmail(homer.email); 
		assertEquals(user2, homer);
		assertEquals (users.size()+1, pacemaker.getUsers().size());
	  }
	  
	  @Test
	  public void testListUsers()
	  {
		assertEquals (users.size(), pacemaker.getUsers().size());
		users.forEach(
				user -> pacemaker.getUserByEmail(user.email));
		service.listUsers();
		assertEquals (users.size(), pacemaker.getUsers().size());

	  }
	  
	  @Test
	  public void testLogin()
	  {
		  service.login("marge@simpson.com", "secret");
		  User loggedInUser = pacemaker.getUserByEmail("marge@simpson.com");
		  Optional<User> user = Optional.fromNullable(loggedInUser);
		  assertEquals(user.isPresent(), true);
	  }
	  
	  @Test
	  public void testLogout()
	  {
		  service.login("marge@simpson.com", "secret");
		  User loggedInUser = pacemaker.getUserByEmail("marge@simpson.com");
		  Optional<User> user = Optional.fromNullable(loggedInUser);
		  assertEquals(user.isPresent(), true);
		  service.logout();
		  assertEquals(user.isPresent(), false);
	  }
	  
	  
}
