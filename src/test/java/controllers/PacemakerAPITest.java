package controllers;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import controllers.PacemakerAPI;
import models.Activity;
import models.Location;
import models.User;

import static models.Fixtures.users;
import static models.Fixtures.activities;
import static models.Fixtures.locations;

public class PacemakerAPITest
{
  private PacemakerAPI pacemaker;

  @Before
  public void setup()
  {
	PacemakerAPI pacemaker = new PacemakerAPI("http://localhost:7000");
    pacemaker.deleteUsers();
    users.forEach(user -> pacemaker.createUser(user.firstname, user.lastname, user.email, user.password));
    
  }

  @After
  public void tearDown()
  {
    pacemaker = null;
  }

  @Test
  public void testUser()
  {
    assertEquals (users.size(), pacemaker.getUsers().size());
    pacemaker.createUser("homer", "simpson", "homer@simpson.com", "secret");
    assertEquals (users.size()+1, pacemaker.getUsers().size());
    assertEquals (users.get(0), pacemaker.getUserByEmail(users.get(0).email));
  }  

  @Test
  public void testUsers()
  {
    assertEquals (users.size(), pacemaker.getUsers().size());
    for (User user: users)
    {
      User eachUser = pacemaker.getUserByEmail(user.email);
      
      assertEquals (user, eachUser);
      assertNotSame(user, eachUser);
    }
  }

  @Test
  public void testDeleteUsers()
  {
    assertEquals (users.size(), pacemaker.getUsers().size());
    User marge = pacemaker.getUserByEmail("marge@simpson.com");
    pacemaker.deleteUser(marge.id);
    assertEquals (users.size()-1, pacemaker.getUsers().size());    
  }  
  
  @Test
  public void testAddActivity()
  {
    User marge = pacemaker.getUserByEmail("marge@simpson.com");
    Activity activity = pacemaker.createActivity(marge.id, activities.get(0).type, activities.get(0).location, activities.get(0).distance);
    Activity returnedActivity = pacemaker.getActivity(activity.id);
    assertEquals(activities.get(0),  returnedActivity);
    assertNotSame(activities.get(0), returnedActivity);
  }
  
  @Test
  public void testCreateActivityWithSingleLocation() {
	User homer = pacemaker.getUserByEmail("homer@simpson.com");
    pacemaker.deleteActivities(homer.id);
    Activity activity = new Activity("walk", "shop", 2.5);
    Location location = new Location(12.0, 33.0);

    Activity returnedActivity = pacemaker.createActivity(homer.id, activity.type, activity.location, activity.distance);
    pacemaker.addLocation(homer.id, returnedActivity.id, location.latitude, location.longitude);

    List<Location> locations = pacemaker.getLocations(homer.id, returnedActivity.id);
    assertEquals (locations.size(), 1);
    assertEquals (locations.get(0), location);
  }
  
  @Test
  public void testCreateActivityWithMultipleLocation() {
	User homer = pacemaker.getUserByEmail("homer@simpson.com");
    pacemaker.deleteActivities(homer.id);
    Activity activity = new Activity("walk", "shop", 2.5);
    Activity returnedActivity = pacemaker.createActivity(homer.id, activity.type, activity.location, activity.distance);

    locations.forEach (location ->  pacemaker.addLocation(homer.id, returnedActivity.id, location.latitude, location.longitude));
    List<Location> returnedLocations = pacemaker.getLocations(homer.id, returnedActivity.id);
    assertEquals (locations.size(), returnedLocations.size());
    assertEquals(locations, returnedLocations);
  }
}

