package controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import models.Activity;
import models.User;
import static models.Fixtures.activities;
import static models.Fixtures.locations;

public class ActivityTest {

  PacemakerAPI pacemaker = new PacemakerAPI("http://localhost:7000");
  User homer = new User("homer", "simpson", "homer@simpson.com", "secret");

  @Before
  public void setup() {
    pacemaker.deleteUsers();
    homer = pacemaker.createUser(homer.firstname, homer.lastname, homer.email, homer.password);
    Activity test = new Activity ("walk",  "fridge", 0.001);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testCreateActivity() {
    Activity activity = new Activity("walk", "shop", 2.5);

    Activity returnedActivity = pacemaker.createActivity(homer.id, activity.type, activity.location, activity.distance);
    assertEquals(activity.type, returnedActivity.type);
    assertEquals(activity.location, returnedActivity.location);
    assertEquals(activity.distance, returnedActivity.distance, 0.001);
    assertNotNull(returnedActivity.id);
  }
  
  @Test
  public void testIds()
  {
    Set<String> ids = new HashSet<>();
    activities.forEach(
    activity -> pacemaker.createActivity(homer.id, activity.type, activity.location, activity.distance));
    Collection<Activity> returnedActivities = pacemaker.getActivities(homer.id);
    assertEquals (activities.size(), ids.size());
  }
  
 
  @Test
  public void testToString()
  {
    assertEquals("Activity{" + activities.get(0).id + ", walk, fridge, 0.001}", activities.get(0).toString());
  }

  
  @Test
  public void testGetActivityDetails()
  {
    assertEquals ("walk", activities.get(0).getType());
    assertEquals ("fridge", activities.get(0).getLocation());
    assertEquals (0.001, activities.get(0).getDistance());    
  }
}